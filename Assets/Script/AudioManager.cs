using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private AudioSource m_Music;
    [SerializeField] private AudioSource m_SFX;
    [SerializeField] private AudioSource m_EnemySFX;
    [SerializeField] private AudioSource m_Echo;
    [SerializeField] private GameObject m_VolumeButton;

    [SerializeField] private Sprite m_MutedIcon, m_UnmutedIcon;

    [SerializeField] private AudioClip m_Intro;
    [SerializeField] private AudioClip m_BattleMusicClip;
    [SerializeField] private AudioClip m_VictoryMusicClip;
    [SerializeField] private AudioClip m_LaserSFXClip;
    [SerializeField] private AudioClip m_EggSGXClip;
    [SerializeField] private AudioClip m_HitSFXClip;
    [SerializeField] private AudioClip m_PlayerHitSFXClip;
    [SerializeField] private AudioClip m_ExplosionSFXClip;
    [SerializeField] private AudioClip m_ClickSFXClip;
    [SerializeField] private AudioClip m_PauseMusicClip;

    private static AudioManager m_Instance;
    private bool m_IsMuted;

    private void Start()
    {
        m_IsMuted = false;
    }

    public static AudioManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<AudioManager>();
                return m_Instance;
            }
            else return m_Instance;
        }
    }

    private void Awake()
    {
        if(m_Instance == null)
        {
            m_Instance = this;
        }
        else if(m_Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void PlayIntro()
    {
        m_Music.loop = true;
        m_Music.clip = m_Intro;
        m_Music.Play();
    }

    public void PlayPauseMusic()
    {
        m_Music.loop = true;
        m_Music.clip = m_PauseMusicClip;
        m_Music.Play();
    }

    public void PlayBattleMusic()
    {
        m_Music.loop = true;
        m_Music.clip = m_BattleMusicClip;
        m_Music.Play();
    }

    public void PlayVictoryMusic()
    {
        m_Music.loop = true;
        m_Music.clip = m_VictoryMusicClip;
        m_Music.Play();
    }

    public void PlayLaserSFXClip()
    {
        m_SFX.PlayOneShot(m_LaserSFXClip);
    }

    public void PlayHitSFXClip()
    {
        m_EnemySFX.PlayOneShot(m_HitSFXClip);
    }
    public void PlayEggSGXClip()
    {
        m_EnemySFX.PlayOneShot(m_EggSGXClip);
    }
    public void PlayExplosionSFXClip()
    {
        m_Echo.PlayOneShot(m_ExplosionSFXClip);
    }

    public void SetMusicVol(float vol)
    {
        m_Music.volume = vol;
    }

    public void PlayClickSFXClip()
    {
        m_SFX.PlayOneShot(m_ClickSFXClip);
    }

    public void PlayPlayerHitSFXClip()
    {
        m_SFX.PlayOneShot(m_PlayerHitSFXClip);
    }

    public void m_OnVolumeButton_Pressed()
    {
        if (!m_IsMuted)
        {
            m_IsMuted = true;
            m_Music.mute = !m_Music.mute;
            m_SFX.mute = !m_SFX.mute;
            m_EnemySFX.mute = !m_EnemySFX.mute;
            m_Echo.mute = !m_Echo.mute;
            m_VolumeButton.GetComponent<Image>().sprite = m_MutedIcon;
        }
        else
        {
            m_IsMuted = false;
            m_Music.mute = !m_Music.mute;
            m_SFX.mute = !m_SFX.mute;
            m_EnemySFX.mute = !m_EnemySFX.mute;
            m_Echo.mute = !m_Echo.mute;
            m_VolumeButton.GetComponent<Image>().sprite = m_UnmutedIcon;
        }
    }
}
