using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Section3
{ 
    public class HomePanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_TxtHighScore;

        // Start is called before the first frame update

        public void OnEnable()
        {
            m_TxtHighScore.text = "High Score: " + PlayerPrefs.GetInt("HighScore");
        }

        public void BtnPlay_Pressed()
        {
            AudioManager.Instance.PlayClickSFXClip();
            GameManager.Instance.Play();
        }

    
    }
}
