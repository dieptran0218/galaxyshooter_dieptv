using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Section3 {
    public class PausePanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_TxtScore;
        
        // Start is called before the first frame update

        public void BtnResume_Pressed()
        {
            AudioManager.Instance.PlayClickSFXClip();
            GameManager.Instance.Resume();
        }

        public void BtnHome_Pressed()
        {
            AudioManager.Instance.PlayClickSFXClip();
            GameManager.Instance.Home();
        }
        public void DisplayScore(int score)
        {
            m_TxtScore.text = "Your score: " + score;
        }

    }
}