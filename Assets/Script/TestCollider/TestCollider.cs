using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCollider : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("OnTriggerEnter2D " + collision.name + "___" + gameObject.name);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {

        Debug.Log("OnCollisionEnter2D " + collision.gameObject.name + "___" + gameObject.name);
    }
}
