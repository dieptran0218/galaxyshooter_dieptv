using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private float m_MoveSpeed;
        [SerializeField] private Transform[] m_WayPoints;

        [SerializeField] private ProjectileController m_Projectile;
        [SerializeField] private Transform m_FiringPoint;
        [SerializeField] private float m_MaxCooldown, m_MinCooldown;
        [SerializeField] public int m_HP;
        [SerializeField] private GameObject m_RightBorderPoint;
        [SerializeField] private GameObject m_LeftBorderPoint;
        [SerializeField] private GameObject m_UpBorderPoint;
        [SerializeField] private GameObject m_BottomBorderPoint;



        private float m_TempCooldown;
        private int m_CurentWayPointIndex;
        private float m_CurrentMoveSpeed;
        private bool m_Active;
        private int m_TempHP;
        private int m_SpeedMultiplier;

        public 

        // Start is called before the first frame update
        void Start()
        {

            m_TempCooldown = Random.Range(m_MinCooldown, m_MaxCooldown);
            m_TempHP = m_HP;
        }

        // Update is called once per frame
        void Update()
        {
            

            if (!m_Active)
            {
                return;
            }
            int nextWayPoint = m_CurentWayPointIndex + 1;
            if (nextWayPoint > m_WayPoints.Length - 1)
            {
                //Debug.Log("ok");
                nextWayPoint = 0;
            }
            transform.position = Vector3.MoveTowards(transform.position,m_WayPoints[nextWayPoint].position, m_CurrentMoveSpeed * Time.deltaTime);

            //Debug.Log(Vector2.Distance(transform.position, m_WayPoints[nextWayPoint].position));
           // if(transform.position.x == m_WayPoints[nextWayPoint].position.x && transform.position.y == m_WayPoints[nextWayPoint].position.y)
            if(Vector2.Distance(transform.position, m_WayPoints[nextWayPoint].position)< 0.01f)
            {
                //Debug.Log("ok 1");
                m_CurentWayPointIndex = nextWayPoint;
             }

            Vector3 direction = m_WayPoints[nextWayPoint].position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle-90, Vector3.forward);

            if (m_TempCooldown <= 0)
            {
                //Debug.Log(m_TempAngle);
                //ProjectileController Egg = Instantiate(m_Projectile, m_FiringPoint.position, Quaternion.identity, null);

                SpawnManager.Instance.EnemyFire(m_FiringPoint, IsInCamera(), m_SpeedMultiplier);
                m_TempCooldown = Random.Range(m_MinCooldown, m_MaxCooldown) / m_SpeedMultiplier;
            }
            m_TempCooldown -= Time.deltaTime;
        }

        public void Init(Transform[] WayPoints, int SpeedMultiplier)
        {
            m_WayPoints = WayPoints;
            m_SpeedMultiplier = SpeedMultiplier;
            m_Active = true;
            m_CurentWayPointIndex = 0;
            m_CurrentMoveSpeed = m_MoveSpeed * m_SpeedMultiplier;
            m_TempCooldown = Random.Range(m_MinCooldown, m_MaxCooldown) / m_SpeedMultiplier;
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("PlayerProjectile"))
            {
                ProjectileController Laser;
                collision.TryGetComponent(out Laser);
                //Debug.Log("OK");
                SpawnManager.Instance.ReleaseProjectile(true, Laser);
            }

            if (collision.gameObject.CompareTag("Player"))
            {
                PlayerController player;
                collision.TryGetComponent(out player);

                player.Hit();
            }
        }

        public void Hit(int damage)
        {
            m_TempHP -= damage;
            if (m_TempHP <= 0)
            {
                GameManager.Instance.AddScore();
                AudioManager.Instance.PlayHitSFXClip();
                SpawnManager.Instance.ReleaseEnemy(this);
                SpawnManager.Instance.SpawnEnemyExplosionFX(transform.position);
            }
        }

        public bool IsInCamera()
        {

            if (transform.position.x > m_RightBorderPoint.transform.position.x ||
                transform.position.x < m_LeftBorderPoint.transform.position.x ||
                transform.position.y > m_UpBorderPoint.transform.position.y ||
                transform.position.y < m_BottomBorderPoint.transform.position.y)
            {
                return false;
            }

            return true;
        }

        public void ResetHP()
        {
            m_TempHP = m_HP;
        }
    }
}