using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] float MoveSpeed;

        [SerializeField] private ProjectileController m_Projectile;
        
        [SerializeField] private float m_FiringCooldown;

        [SerializeField] private GameObject m_RightBorderPoint;
        [SerializeField] private GameObject m_LeftBorderPoint;
        [SerializeField] private GameObject m_UpBorderPoint;
        [SerializeField] private GameObject m_BottomBorderPoint;
        [SerializeField] private int m_HP, m_TempHP;

        private float m_TempCooldown;
        private SpriteRenderer m_PlayerSprite;
        public Action<int, int> onHPChanged;

        public static PlayerController Instance;

        //private Vector2 m_MovementInputValue;
        //private bool m_AttackInputValue;

        //private void OnEnable()
        //{
        //    if(m_PlayerInput == null)
        //    {
        //        m_PlayerInput = new PlayerInput();
        //        m_PlayerInput.Player.Movement.started += OnMovement;
        //        m_PlayerInput.Player.Movement.performed += OnMovement;
        //        m_PlayerInput.Player.Movement.canceled += OnMovement;

        //        m_PlayerInput.Player.Attack.started += OnAttack;
        //        m_PlayerInput.Player.Attack.performed += OnAttack;
        //        m_PlayerInput.Player.Attack.canceled += OnAttack;

        //        m_PlayerInput.Enable();
        //    }
        //    else m_PlayerInput.Enable();
        //}

        private void Awake()
        {
            Instance = this;
        }

        // Start is called before the first frame update
        void Start()
        {
            m_PlayerSprite = GetComponent<SpriteRenderer>();
            m_TempHP = m_HP;
            if(onHPChanged != null)
            {
                onHPChanged(m_TempHP,m_HP);
            }
        }

        // Update is called once per frame
        void Update()
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            Vector2 direction = new Vector2(horizontal, vertical);

            transform.Translate(direction * Time.deltaTime * MoveSpeed);
#if PLATFORM_ANDROID
            if (m_TempCooldown <= 0)
            {

                //Debug.Log(m_TempAngle);
                SpawnManager.Instance.PlayerFire();
                m_TempCooldown = m_FiringCooldown;
            }
            m_TempCooldown -= Time.deltaTime;
#elif UNITY_EDITOR
            if (Input.GetKey(KeyCode.Space))
            {
                if (m_TempCooldown <= 0)
                {
                    
                    //Debug.Log(m_TempAngle);
                    SpawnManager.Instance.PlayerFire();
                    m_TempCooldown = m_FiringCooldown;
                }
                m_TempCooldown -= Time.deltaTime;
            }
#endif

            CorrectPosition();
        }

        private void CorrectPosition()
        {

            if (gameObject.transform.position.x > m_RightBorderPoint.transform.position.x)
            {
                gameObject.transform.position = new Vector3(m_RightBorderPoint.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            }

            if (gameObject.transform.position.x < m_LeftBorderPoint.transform.position.x)
            {
                gameObject.transform.position = new Vector3(m_LeftBorderPoint.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            }

            if (gameObject.transform.position.y > m_UpBorderPoint.transform.position.y)
            {
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, m_UpBorderPoint.transform.position.y, gameObject.transform.position.z);
            }

            if (gameObject.transform.position.y < m_BottomBorderPoint.transform.position.y)
            {
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, m_BottomBorderPoint.transform.position.y, gameObject.transform.position.z);
            }
        }

        public bool CheckInCamera(Transform transform)
        {

            if (transform.position.x > m_RightBorderPoint.transform.position.x ||
                transform.position.x < m_LeftBorderPoint.transform.position.x ||
                transform.position.y > m_UpBorderPoint.transform.position.y ||
                transform.position.y < m_BottomBorderPoint.transform.position.y)
            {
                return false;
            }
            
            return true;
        }
        

        public void Hit()
        {
            --m_TempHP;
            if (onHPChanged != null)
            {
                onHPChanged(m_TempHP, m_HP);
            }

            if (m_TempHP == 0)
            {
                gameObject.SetActive(false);
                SpawnManager.Instance.SpawnPlayerHitFX(transform.position);
                SpawnManager.Instance.StopSpawnEnemy();
                Invoke("ChangeState", 1.5f);

                AudioManager.Instance.PlayExplosionSFXClip();
            }
            else
            {
                AudioManager.Instance.PlayPlayerHitSFXClip();
                m_PlayerSprite.color = Color.red;
                Invoke("ResetColor", 0.1f);
            }
                      
        }

        public void ResetHP()
        {
            m_TempHP = m_HP;
            if (onHPChanged != null)
            {
                onHPChanged(m_TempHP, m_HP);
            }
        }

        public void ChangeState()
        {
            GameManager.Instance.GameOver(false);
        }

        public void ResetColor()
        {
            m_PlayerSprite.color = Color.white;
        }


        //private void OnAttack(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        //{
        //    if (obj.started)
        //    {
        //        m_AttackInputValue = true;
        //    }

        //    if (obj.performed)
        //    {
        //        m_AttackInputValue = true;
        //    }

        //    if (obj.canceled)
        //    {
        //        m_AttackInputValue = false;
        //    }
        //}

        //private void OnMovement(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        //{
        //    if (obj.started)
        //    {
        //        m_MovementInputValue = obj.ReadValue<Vector2>();
        //    }

        //    if (obj.performed)
        //    {
        //        m_MovementInputValue = obj.ReadValue<Vector2>();
        //    }

        //    if (obj.canceled)
        //    {
        //        m_MovementInputValue = Vector2.zero;
        //    }
        //}
    }
}
