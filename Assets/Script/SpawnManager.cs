using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    [System.Serializable]
    public class EnemyPool
    {
        [SerializeField] private EnemyController prefab;
        [SerializeField] private List<EnemyController> InactiveObj;
        [SerializeField] private List<EnemyController> ActiveObj;

        public EnemyController Spawn(Vector3 position, Transform parent)
        {
            if (InactiveObj.Count == 0)
            {
                EnemyController newObj = GameObject.Instantiate(prefab, parent);
                newObj.transform.position = position;
                ActiveObj.Add(newObj);
                return newObj;
            }
            else
            {
                EnemyController oldObj = InactiveObj[0];
                oldObj.gameObject.SetActive(true);
                oldObj.ResetHP();
                oldObj.transform.SetParent(parent);
                oldObj.transform.position = position;
                ActiveObj.Add(oldObj);
                InactiveObj.Remove(oldObj);
                return oldObj;
            }

        }
         public void Release(EnemyController obj) 
         { 
            if (ActiveObj.Contains(obj)) 
            {
                ActiveObj.Remove(obj);
                InactiveObj.Add(obj);
                obj.gameObject.SetActive(false);
            }
            
         }

        public List<EnemyController> GetActiveList()
        {
            return ActiveObj;
        }
    }

    [System.Serializable]
    public class ProjectilePool
    {
        private ProjectileController m_Projectile;
        [SerializeField] private List<ProjectileController> m_ActiveProj;
        [SerializeField] private List<ProjectileController> m_InactiveProj;

        public ProjectileController Spawn(ProjectileController Projectile, Vector3 position, Quaternion direction)
        {
            if (m_InactiveProj.Count == 0)
            {
                ProjectileController newObj = GameObject.Instantiate(Projectile, position, direction, null);
                newObj.SetTempLifeTime(newObj.GetLifeTime());
                newObj.SetSpeed(1);
                newObj.transform.position = position;
                m_ActiveProj.Add(newObj);
                return newObj;
            }
            else
            {
                ProjectileController oldObj = m_InactiveProj[0];
                oldObj.SetTempLifeTime(oldObj.GetLifeTime());
                oldObj.gameObject.SetActive(true);
                oldObj.transform.position = position;
                oldObj.transform.rotation = direction;
                m_ActiveProj.Add(oldObj);
                m_InactiveProj.Remove(oldObj);
                return oldObj;
            }

            
        }

        public List<ProjectileController> GetActiveList()
        {
            return m_ActiveProj;
        }

        public void Release(ProjectileController obj)
        {
            if (m_ActiveProj.Contains(obj))
            {
                m_ActiveProj.Remove(obj);
                m_InactiveProj.Add(obj);
                obj.gameObject.SetActive(false);
            }

        }
        
    }

    [System.Serializable]
    public class ParticleFXPool
    {
        private ParticleFX m_Projectile;
        [SerializeField] private List<ParticleFX> m_ActiveFX;
        [SerializeField] private List<ParticleFX> m_InactiveFX;

        public ParticleFX Spawn(ParticleFX FX, Vector3 position, Quaternion direction)
        {
            if (m_InactiveFX.Count == 0)
            {
                ParticleFX newObj = GameObject.Instantiate(FX, position, direction, null);
                newObj.ResetLifeTime();
                newObj.transform.position = position;
                m_ActiveFX.Add(newObj);
                return newObj;
            }
            else
            {
                ParticleFX oldObj = m_InactiveFX[0];
                oldObj.ResetLifeTime();
                oldObj.gameObject.SetActive(true);
                oldObj.transform.position = position;
                oldObj.transform.rotation = direction;
                m_ActiveFX.Add(oldObj);
                m_InactiveFX.Remove(oldObj);
                return oldObj;
            }


        }

        public List<ParticleFX> GetActiveList()
        {
            return m_ActiveFX;
        }

        public void Release(ParticleFX obj)
        {
            if (m_ActiveFX.Contains(obj))
            {
                m_ActiveFX.Remove(obj);
                m_InactiveFX.Add(obj);
                obj.gameObject.SetActive(false);
            }

        }
    }


    public class SpawnManager : MonoBehaviour
    {
        [SerializeField] private Transform m_PlayerFiringPoint;
        //[SerializeField] private Transform m_EnemyFiringPoint;
        Quaternion m_RightRotation, m_LeftRotation;
        [SerializeField] private bool m_Active;
        //[SerializeField] private EnemyController EnemyPrefabs;
        [SerializeField] private ProjectilePool m_PlayerProjectilePool;
        [SerializeField] private ProjectilePool m_EnemyProjectilePool;
        [SerializeField] private ProjectileController m_PlayerProjectile;
        [SerializeField] private ProjectileController m_EnemyProjectile;
        [SerializeField] private ParticleFX m_HitFX, m_PlayerHitFX, m_EnemyExplosionFX;
        [SerializeField] private ParticleFX m_ShootingFX;
        [SerializeField] private EnemyPool m_EnemyPool;
        
        [SerializeField] private int m_TotalEnemies, m_DestroyedEnemy;
        private int m_TotalGroups;
        private int m_SpeedMultiplier;
        [SerializeField] private float m_SpawnEnemyInterval;
        [SerializeField] private EnemyPath[] m_EnemyPath;
        [SerializeField] private GameObject m_SpawnPoint;
        [SerializeField] private float m_Angle, m_TempAngle, m_AngleStatus;
        [SerializeField] private ParticleFXPool m_PlayerHitFXPool, m_HitFXPool, m_ShootingFXPool, m_EnemyExplosionFXPool;

        [SerializeField] private PlayerController m_Player;
        [SerializeField] private GameObject m_PlayerRespawnPoint, m_BottomBorderPoint;

        private WaveData m_CurrentWave;
        public static SpawnManager m_Instance;
        public PlayerController Player => m_Player;

        public static SpawnManager Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = FindObjectOfType<SpawnManager>();
                    return m_Instance;
                }
                else return m_Instance;
            }
        }

        private void Awake()
        {
            if (m_Instance == null)
            {
                m_Instance = this;
            }
            else if (m_Instance != this)
            {
                Destroy(gameObject);
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            m_RightRotation = new Quaternion(0, 0, 0, 1);
            m_LeftRotation = new Quaternion(0, 0, 0, 1);
        }

        private IEnumerator IESpawnEnemies()
        {
            yield return new WaitUntil(()=>m_Active);
            int PathIndex = 0;
            for(int i= 0; i< m_TotalGroups; i++)
            {
                PathIndex = UnityEngine.Random.Range(0,m_EnemyPath.Length);
                for (int j = 0; j < m_TotalEnemies; j++)
                {
                    //EnemyController enemy = Instantiate(EnemyPrefabs,m_SpawnPoint.transform.position, Quaternion.identity, null);
                    EnemyController enemy = m_EnemyPool.Spawn(m_SpawnPoint.transform.position, transform);
                    enemy.Init(m_EnemyPath[PathIndex].WayPoints, m_SpeedMultiplier);
                    yield return new WaitForSeconds(m_SpawnEnemyInterval / m_SpeedMultiplier);
                }
                yield return new WaitForSeconds(2f / m_SpeedMultiplier);
            }


            //for (int j = 0; j < m_EnemyPath.Length; j++)
            ////for (int j = 0; j < 1; j++)
            //{
            //    for (int i = 0; i < m_TotalEnemies; i++)
            //    {
            //        //EnemyController enemy = Instantiate(EnemyPrefabs,m_SpawnPoint.transform.position, Quaternion.identity, null);
            //        EnemyController enemy = m_EnemyPool.Spawn(m_SpawnPoint.transform.position, transform);
            //        enemy.SetWayPoint(m_EnemyPath[j].WayPoints);
            //        yield return new WaitForSeconds(m_SpawnEnemyInterval);
            //    }
            //    yield return new WaitForSeconds(2f);
            //}
        }

        //private IEnumerator IETestCoroutine()
        //{
        //    yield return new WaitUntil(() => m_Active);
        //    for (int i = 0; i < 5; i++)
        //    {
        //        Debug.Log(i);
        //        yield return new WaitForSeconds(1f);
        //    }
        //}

        public void ReleaseEnemy(EnemyController obj)
        {
            m_EnemyPool.Release(obj);
            ++m_DestroyedEnemy;
            if(m_TotalEnemies * m_TotalGroups == m_DestroyedEnemy)
            {
                Invoke("ChangeWave", 0.7f);
            }
        }

        public ProjectileController SpawnPlayerProjectile(Vector3 position, Quaternion direction)
        {
            return m_PlayerProjectilePool.Spawn(m_PlayerProjectile, position, direction);
        }

        public void ReleasePlayerProjectile(ProjectileController obj)
        {
            m_PlayerProjectilePool.Release(obj);
        }

       

        public void ReleaseEnemyProjectile(ProjectileController obj)
        {
            m_EnemyProjectilePool.Release(obj);
        }

        public ProjectileController SpawnEnemyProjectile(Vector3 position, Quaternion direction)
        {
            return m_EnemyProjectilePool.Spawn(m_EnemyProjectile, position, direction);
        }

        public void PlayerFire()
        {

            m_TempAngle += m_AngleStatus * (float)0.025;
            if (m_TempAngle >= m_Angle || m_TempAngle <= -1 * m_Angle)
            {
                m_AngleStatus *= -1;
            }
            m_LeftRotation.z = -1 * m_TempAngle;
            m_RightRotation.z = m_TempAngle;

            AudioManager.Instance.PlayLaserSFXClip();
            ProjectileController MiddleProjectile = SpawnPlayerProjectile(m_PlayerFiringPoint.position, Quaternion.identity);
            MiddleProjectile.SetFromPlayer(true);

            //ProjectileController LeftProjectile = SpawnPlayerProjectile(m_PlayerFiringPoint.position, m_LeftRotation);
            //LeftProjectile.SetFromPlayer(true);

            //ProjectileController RightProjectile = SpawnPlayerProjectile(m_PlayerFiringPoint.position, m_RightRotation);
            //RightProjectile.SetFromPlayer(true);

            SpawnManager.Instance.SpawnShootingFX(m_PlayerFiringPoint.position);
        }

        public void EnemyFire(Transform FiringPoint, bool IsInCamera, int SpeedMultiplier)
        {
            if(IsInCamera) AudioManager.Instance.PlayEggSGXClip();
            ProjectileController enemyProjectile = SpawnEnemyProjectile(FiringPoint.position, Quaternion.identity);
            enemyProjectile.SetFromPlayer(false);
            enemyProjectile.SetSpeed(SpeedMultiplier);
        }

        internal void ReleaseProjectile(bool fromPlayer, ProjectileController projectile)
        {
            if (fromPlayer)
            {
                ReleasePlayerProjectile(projectile);
            }
            else
            {
                ReleaseEnemyProjectile(projectile);
            }
        }
        public ParticleFX SpawnHitFX(Vector3 position)
        {
            ParticleFX fx = m_HitFXPool.Spawn(m_HitFX,position,Quaternion.identity);
            fx.SetPool(m_HitFXPool);
            return fx;
        }

        public ParticleFX SpawnPlayerHitFX(Vector3 position)
        {
            ParticleFX fx = m_PlayerHitFXPool.Spawn(m_PlayerHitFX, position, Quaternion.identity);
            fx.SetPool(m_PlayerHitFXPool);
            return fx;
        }

        public void ReleasePlayerHitFX(ParticleFX HitFX)
        {
            m_PlayerHitFXPool.Release(HitFX);
        }

        public void ReleaseHitFX(ParticleFX HitFX)
        {
            m_HitFXPool.Release(HitFX);
        }

        public ParticleFX SpawnEnemyExplosionFX(Vector3 position)
        {
            ParticleFX fx = m_EnemyExplosionFXPool.Spawn(m_EnemyExplosionFX, position, Quaternion.identity);
            fx.SetPool(m_EnemyExplosionFXPool);
            return fx;
        }
        public void ReleaseEnemyExplosionFX(ParticleFX EnemyExplosionFX)
        {
            m_EnemyExplosionFXPool.Release(EnemyExplosionFX);
        }

        public ParticleFX SpawnShootingFX(Vector3 position)
        {
            ParticleFX fx = m_ShootingFXPool.Spawn(m_ShootingFX, position, Quaternion.identity);
            fx.SetPool(m_ShootingFXPool);
            return fx;
        }
        public void ReleaseShootingFX(ParticleFX ShootingFX)
        {
            m_ShootingFXPool.Release(ShootingFX);
        }

        public void StopSpawnEnemy()
        {
            StopAllCoroutines();

            List<EnemyController> enemyActiveList = m_EnemyPool.GetActiveList();
            for (int i = enemyActiveList.Count - 1 ; i >= 0 ; i--)
            {
                ReleaseEnemy(enemyActiveList[i]);
            }



            List<ProjectileController> projectileActiveList = m_EnemyProjectilePool.GetActiveList();
            for (int i = projectileActiveList.Count - 1; i >= 0; i--)
            {
                ReleaseEnemyProjectile(projectileActiveList[i]);
            }


        }

        public void RepsawnEnemy(WaveData wave)
        {
            m_CurrentWave = wave;
            m_TotalEnemies = wave.totalEnemy;
            m_TotalGroups = wave.totalGroups;
            m_SpeedMultiplier = wave.speedMultiplier;
            m_DestroyedEnemy = 0;
            StartCoroutine(IESpawnEnemies());        
        }

        public void RespawnPlayer()
        {
            m_Player.gameObject.SetActive(true);
            m_Player.ResetHP();
            m_Player.gameObject.transform.position = m_PlayerRespawnPoint.gameObject.transform.position;
            //while (m_Player.transform.position.x < m_PlayerRespawnPoint.transform.position.x) {
            //    m_Player.transform.position = Vector3.MoveTowards(m_Player.gameObject.transform.position, m_PlayerRespawnPoint.transform.position, 2 * Time.deltaTime);
            //}
        }

        public void ChangeWave()
        {
            if (!GameManager.Instance.NextWave())
            {
                Victory();
            }
        }

        public void Victory()
        {
            GameManager.Instance.GameOver(true);
        }

    }
}