using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

namespace Section3
{
    public class GameOverPanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_TxtResult;
        [SerializeField] private TextMeshProUGUI m_TxtHighScore;

        

        // Start is called before the first frame update


        public void BtnHome_Pressed()
        {
            AudioManager.Instance.PlayClickSFXClip();
            GameManager.Instance.Home();
            
        }

        public void BtnPlayAgain_Pressed()
        {
            AudioManager.Instance.PlayClickSFXClip();
            GameManager.Instance.Play();
        }

        public void DisplayHighScore(int score)
        {
            m_TxtHighScore.text = "New high score : " + score;
        }



        public void DisplayResult(bool isWin)
        {
            if (isWin)
            {
                m_TxtResult.text = "You win!";
            }
            else
            {
                m_TxtResult.text = "Better luck next time!";
            }
        }

        public void DisplayNewScore(int m_CurrentHighScore)
        {
            m_TxtHighScore.text = "Your score: " + m_CurrentHighScore;
        }
    }
}