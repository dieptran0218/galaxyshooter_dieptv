using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    
    public class BackgroundController : MonoBehaviour
    {
        [SerializeField] private Material m_BigStarsBg;
        [SerializeField] private Material m_MedStarsBg;
        [SerializeField] private Material m_BackgroundBg;
        [SerializeField] private float m_BigStarsScrollSpeed;
        [SerializeField] private float m_MedStarsScrollSpeed;
        [SerializeField] private float m_BackgroundScrollSpeed;


        private int m_MainTexId;
        // Start is called before the first frame update
        void Start()
        {
            m_MainTexId = Shader.PropertyToID("_MainTex");
        }

        // Update is called once per frame
        void Update()
        {
            Vector2 offset = m_BigStarsBg.GetTextureOffset(m_MainTexId);
            offset += new Vector2(0, m_BigStarsScrollSpeed * Time.deltaTime);
            m_BigStarsBg.SetTextureOffset(m_MainTexId, offset);

            offset = m_MedStarsBg.GetTextureOffset(m_MainTexId);
            offset += new Vector2(0, m_MedStarsScrollSpeed * Time.deltaTime);
            m_MedStarsBg.SetTextureOffset(m_MainTexId, offset);

            offset = m_BackgroundBg.GetTextureOffset(m_MainTexId);
            offset += new Vector2(0, m_BackgroundScrollSpeed * Time.deltaTime);
            m_BackgroundBg.SetTextureOffset(m_MainTexId, offset);
        }
    }
}