using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    public enum GameState
    {
        Home,
        GamePlay,
        Pause,
        GameOver
    }

    public class GameManager : MonoBehaviour
    {
        private GameState m_GameState;

        [SerializeField] private HomePanel m_HomePanel;
        [SerializeField] private GameplayPanel m_GameplayPanel;
        [SerializeField] private PausePanel m_PausePanel;
        [SerializeField] private GameOverPanel m_GameOverPanel;
        [SerializeField] private WaveData[] m_WaveData;

        private int m_CurrentWaveIndex;

        public int m_Score;
        public bool m_IsWin;

        public static GameManager m_Instance;

        public static GameManager Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = FindObjectOfType<GameManager>();
                    return m_Instance;
                }
                else return m_Instance;
            }
        }

        private void Awake()
        {
            if (m_Instance == null)
            {
                m_Instance = this;
            }
            else if (m_Instance != this)
            {
                Destroy(gameObject);
            }
        }

        public Action<int> OnScoreChanged;
        public Action<int> OnLevelChanged;
        private int m_CurrentHighScore;

        // Start is called before the first frame update
        void Start()
        {
            m_HomePanel.gameObject.SetActive(false);
            m_GameplayPanel.gameObject.SetActive(false);
            m_PausePanel.gameObject.SetActive(false);
            m_GameOverPanel.gameObject.SetActive(false);
            SetStage(GameState.Home);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void SetStage(GameState stage)
        {
            m_GameState = stage;
            m_HomePanel.gameObject.SetActive(stage == GameState.Home);
            m_GameplayPanel.gameObject.SetActive(stage == GameState.GamePlay);
            m_PausePanel.gameObject.SetActive(stage == GameState.Pause);
            m_GameOverPanel.gameObject.SetActive(stage == GameState.GameOver);

            if (m_GameState == GameState.Pause)
            {
                
                Time.timeScale = 0;
                AudioManager.Instance.PlayPauseMusic();
                AudioManager.Instance.SetMusicVol(0.1f);
            }
            else Time.timeScale = 1;

            if (m_GameState == GameState.GamePlay)
            {
                AudioManager.Instance.PlayBattleMusic();
                AudioManager.Instance.SetMusicVol(0.32f);
            }
            else{
                AudioManager.Instance.SetMusicVol(0.1f);
            }

            if (m_GameState == GameState.Home)
            {
                AudioManager.Instance.PlayIntro();
            }

            if(m_GameState == GameState.GameOver)
            {
                if (m_IsWin == true)
                {
                    AudioManager.Instance.PlayVictoryMusic();
                }
                else AudioManager.Instance.PlayIntro();
            }
            

        }

        public void Play()
        {
            m_CurrentWaveIndex = 0;
            WaveData wave = m_WaveData[m_CurrentWaveIndex];

            SetStage(GameState.GamePlay);
            SpawnManager.Instance.RespawnPlayer();
            SpawnManager.Instance.RepsawnEnemy(wave);
            m_Score = 0;
            if (OnScoreChanged != null)
            {
                OnScoreChanged(m_Score);
            }

        }

        public void Pause()
        {
            SetStage(GameState.Pause);
            m_PausePanel.DisplayScore(m_Score);
        }
        public void Resume()
        {
            SetStage(GameState.GamePlay);
        }

        public void Home()
        {
            SetStage(GameState.Home);
            SpawnManager.Instance.StopSpawnEnemy();
            SpawnManager.Instance.Player.gameObject.SetActive(false);
        }

        

        public void GameOver(bool isWin)
        {
            m_CurrentHighScore = m_Score;
            m_IsWin = isWin;


            if (m_CurrentHighScore > PlayerPrefs.GetInt("HighScore"))
            {
                PlayerPrefs.SetInt("HighScore",m_CurrentHighScore);
                m_GameOverPanel.DisplayHighScore(m_CurrentHighScore);
            }
            else
            {
                m_GameOverPanel.DisplayNewScore(m_CurrentHighScore);
            }
            
            SpawnManager.Instance.StopSpawnEnemy();
            SetStage(GameState.GameOver);
            
            m_GameOverPanel.DisplayResult(m_IsWin);
        }

        public void AddScore()
        {
            m_Score += 10;
            if(OnScoreChanged != null)
            {
                OnScoreChanged(m_Score);
            }
        }

        public bool NextWave()
        {
            ++m_CurrentWaveIndex;
            if (m_CurrentWaveIndex < m_WaveData.Length)
            {
                WaveData wave = m_WaveData[m_CurrentWaveIndex];
                SpawnManager.Instance.RepsawnEnemy(wave);
                if(OnLevelChanged != null)
                {
                    OnLevelChanged(m_CurrentWaveIndex + 1);
                }
                return true;
            }
            return false;
        }
    }
}