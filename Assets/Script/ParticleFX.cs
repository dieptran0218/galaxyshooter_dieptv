using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    public class ParticleFX : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] private float m_LifeTime;
        private float m_CurrentFileTime;
        private ParticleFXPool m_Pool;

        public void ResetLifeTime()
        {
            m_CurrentFileTime = m_LifeTime;
        }


        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            m_CurrentFileTime -= Time.deltaTime;
            if (m_CurrentFileTime < 0)
            {
                m_Pool.Release(this);
            }
        }

        public void SetPool(ParticleFXPool pool)
        {
            m_Pool = pool;
        }

    }
}