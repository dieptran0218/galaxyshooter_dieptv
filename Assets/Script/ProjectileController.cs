using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Section3
{
    public class ProjectileController : MonoBehaviour
    {
        [SerializeField] private float m_MoveSpeed;
        [SerializeField] private Vector2 m_Direction;
        
        [SerializeField] private float m_LifeTime;
        [SerializeField]private float m_TempLifeTime;

        [SerializeField] private int m_Damage;

        private bool FromPlayer;
        private float m_CurrentSpeed;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            transform.Translate(m_Direction * Time.deltaTime * m_CurrentSpeed);
            m_TempLifeTime -= Time.deltaTime;
            if(m_TempLifeTime < 0)
            {
                SpawnManager.Instance.ReleaseProjectile(FromPlayer, this);
            }
        }


        

        public float GetLifeTime()
        {
            return m_LifeTime;
        }



        public void SetTempLifeTime(float LifeTime)
        {
            m_TempLifeTime = LifeTime;
        }

        void OnTriggerEnter2D(Collider2D collision)
        {
            

            if (collision.gameObject.CompareTag("Enemy"))
            {
                EnemyController enemy;
                //Transform enemyTransform;
                collision.TryGetComponent(out enemy);
                //enemyTransform = enemy.GetComponent<Transform>();
                SpawnManager.Instance.SpawnHitFX(collision.ClosestPoint(transform.position));
                enemy.Hit(m_Damage);

                //if (PositionChecker.CheckInCamera(enemyTransform))
                //{
                //    enemy.Hit(m_Damage);
                //}
                
                
            }

            if (collision.gameObject.CompareTag("Player"))
            {
                
                PlayerController player;
                collision.TryGetComponent(out player);

                SpawnManager.Instance.ReleaseProjectile(FromPlayer, this);

                player.Hit();
            }

            if (collision.gameObject.CompareTag("EnemyProjectile"))
            {
                ProjectileController egg;
                collision.TryGetComponent(out egg);

                SpawnManager.Instance.ReleaseProjectile(FromPlayer, egg);
            }

            if (collision.gameObject.CompareTag("PlayerProjectile"))
            {
                ProjectileController Laser;
                collision.TryGetComponent(out Laser);
                //Debug.Log("OK");
                SpawnManager.Instance.ReleaseProjectile(FromPlayer, Laser);
            }

        }

        public void SetFromPlayer(bool fromPlayer)
        {
            FromPlayer = fromPlayer;
        }

        public void SetSpeed(int speedMultiplier)
        {
            m_CurrentSpeed = m_MoveSpeed * (float)speedMultiplier;
        }

        //void OnBecameInvisible()
        //{
        //    Debug.Log("desstroyed");
        //    PlayerController.m_SpawnManager.ReleaseProjectile(FromPlayer, this);
        //}
    }
}