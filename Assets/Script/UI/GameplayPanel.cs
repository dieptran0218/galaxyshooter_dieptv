using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

namespace Section3
{
    public class GameplayPanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_TxtScore;

        [SerializeField] private Image m_HPBarValue;
        [SerializeField] private TextMeshProUGUI m_TxtLevel;

        // Start is called before the first frame update

        private void OnEnable()
        {
            GameManager.Instance.OnScoreChanged += OnScoreChanged;
            SpawnManager.Instance.Player.onHPChanged += OnHPChanged;
            GameManager.Instance.OnLevelChanged += OnLevelChanged;
        }


        private void OnDisable()
        {
            GameManager.Instance.OnScoreChanged -= OnScoreChanged;
            SpawnManager.Instance.Player.onHPChanged -= OnHPChanged;
            GameManager.Instance.OnLevelChanged -= OnLevelChanged;
        }

        private void OnHPChanged(int arg1, int arg2)
        {
            m_HPBarValue.fillAmount = arg1 / (float)arg2;
        }

        private void OnScoreChanged(int score)
        {
            m_TxtScore.text = "Your score: " + score;
        }

        public void BtnPause_Pressed()
        {
            AudioManager.Instance.PlayClickSFXClip();
            GameManager.Instance.Pause();
        }

        private void OnLevelChanged(int level)
        {
            m_TxtLevel.text = m_TxtLevel.text = "Level: " + level;
            m_TxtLevel.color = Color.green;
            Invoke("ResetLevelColor", 0.5f);
        }

        private void ResetLevelColor()
        {
            m_TxtLevel.color = Color.white;
        }

    }
}