using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Section3
{
    public class DragAndDropController : MonoBehaviour, IDragHandler
    {
        public void OnDrag(PointerEventData eventData)
        {
            Debug.Log("DRAG");
            PlayerController.Instance.transform.localPosition += new Vector3(eventData.delta.x/85, eventData.delta.y/85, 0);
        }
    }
}